#! /usr/bin/env python

import math
import rospy
import matplotlib.pyplot as plt

from std_msgs.msg import Float64
from std_msgs.msg import String
from std_msgs.msg import Empty

from sensor_msgs.msg import Image
from ardrone_autonomy.msg import Navdata

class NavDataLog(object):

    def __init__(self):

        sub_image = rospy.Subscriber("/ardrone/navdata",Navdata, self.ReceiveNavData)

        rospy.init_node('NavDataLog', anonymous=True)
        
        rate = rospy.Rate(10) # 10 Hz
        while not rospy.is_shutdown():
            rate.sleep()

    def ReceiveNavData (self, data):
        rospy.loginfo("Data: %f"%data.vx)
        

if __name__ == '__main__':
    try:
        nav_data_log = NavDataLog()
    except rospy.ROSInterruptException:
        pass
