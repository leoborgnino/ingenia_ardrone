#! /usr/bin/env python

import math
import rospy
import matplotlib.pyplot as plt

from std_msgs.msg import Float64
from std_msgs.msg import String
from std_msgs.msg import Empty

from sensor_msgs.msg import Image
from ardrone_autonomy.msg import Navdata

from cv_bridge import CvBridge

class Image_saver(object):

    def __init__(self):
        self.image_cnt = 0
        self.bridge = CvBridge()

        sub_image = rospy.Subscriber("/ardrone/image_raw",Image, self.ReceiveImage)

        rospy.init_node('save_image', anonymous=True)
        
        rate = rospy.Rate(10) # 10 Hz
        while not rospy.is_shutdown():
            rate.sleep()

    def ReceiveImage (self, data):
        rospy.loginfo("Save %d"%(self.image_cnt))
        
        cv_image = self.bridge.imgmsg_to_cv2(data)

        #plt.imshow(cv_image)
        #plt.savefig('figure%d'%(self.image_cnt))
        self.image_cnt += 1

if __name__ == '__main__':
    try:
        image_saver = Image_saver()
    except rospy.ROSInterruptException:
        pass
