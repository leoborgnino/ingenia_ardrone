#pragma once

/*
Este codigo muestra la camara frontal del drone. Con suerte tambien los
kp y matches.
 */

#ifndef __INTERFAZ_CAMARA_H
#define __INTERFAZ_CAMARA_H

#include "GLWindow2.h"
#include <deque>
#include "sensor_msgs/Image.h"
#include "ardrone_autonomy/Navdata.h"
#include "cvd/thread.h"
#include "cvd/image.h"
#include "cvd/byte.h"
#include "boost/thread.hpp"

class InterfazCamara : private CVD::Thread, private MouseKeyHandler
{
  private:
    GLWindow2* myGLWindow;
    CVD::ImageRef desiredWindowSize;
    CVD::ImageRef defaultWindowSize;
    bool changeSizeNextRender;
    char charBuf[1000];
    std::string msg;

    CVD::Image<CVD::byte> mimFrameBW;
    CVD::Image<CVD::byte> mimFrameBW_workingCopy;
    int mimFrameTime;
    int mimFrameTime_workingCopy;
    unsigned int mimFrameSEQ;
    unsigned int mimFrameSEQ_workingCopy;
    ros::Time mimFrameTimeRos;
    ros::Time mimFrameTimeRos_workingCopy;
    int frameWidth, frameHeight;
    
    // HandleFrame se actualiza cada vez que hay una imagen nueva    
    void HandleFrame();  
    void run();  

    bool keepRunning;
	
    bool lockNextFrame;

    boost::condition_variable  new_frame_signal;
    boost::mutex new_frame_signal_mutex;

  public:
    // Constructor/Destructor
    InterfazCamara();
    ~InterfazCamara(void);

    void startSystem();
    void stopSystem();

    // Estas funciones son callbacks
    void newImage(sensor_msgs::ImageConstPtr img);
    bool newImageAvailable;

};
#endif
