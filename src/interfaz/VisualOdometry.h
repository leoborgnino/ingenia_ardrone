#pragma once

#ifndef __VONODE_H
#define __VONODE_H

#include "interfaz_camara.h"
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Image.h"
#include "tf/tfMessage.h"
#include "tf/transform_listener.h"
#include "tf/transform_broadcaster.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include "ardrone_autonomy/Navdata.h"
#include "std_msgs/String.h"
#include <dynamic_reconfigure/server.h>

class InterfazCamara;

struct VONode
{
  private:
    // Topics
    ros::Subscriber navdata_sub;
    ros::Subscriber vel_sub;
    ros::Subscriber vid_sub;
    ros::Time lastNavStamp;

    std::string navdata_channel;
    std::string control_channel;
    std::string output_channel;
    std::string video_channel;
    std::string command_channel;

    ros::NodeHandle nh_;

  public: 
    InterfazCamara* if_camara;
    
    void Loop();

    VONode();
    ~VONode();

    // ROS message callbacks
    //void navdataCb(const ardrone_autonomy::NavdataConstPtr navdataPtr);
    //void velCb(const geometry_msgs::TwistConstPtr velPtr);
    void vidCb(const sensor_msgs::ImageConstPtr img);

};
#endif
