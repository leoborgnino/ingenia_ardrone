#include "VisualOdometry.h"
#include "ros/ros.h"

unsigned int ros_header_timestamp_base = 0;

int main(int argc, char **argv)
{

  ros::init(argc, argv, "visual_odometry");

  ROS_INFO("Inicializado Nodo Odometria Visual");

  VONode vo_node;

  vo_node.if_camara->startSystem();

  vo_node.Loop();

  vo_node.if_camara->stopSystem();

  return 0;
 }
