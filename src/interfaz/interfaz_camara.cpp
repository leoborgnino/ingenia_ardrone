#include "HelperFunctions.h"
#include "ros/ros.h"
#include "interfaz_camara.h"
#include <cvd/gl_helpers.h>
#include <gvars3/instances.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
#include "GLWindow2.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

InterfazCamara::InterfazCamara ( )
{
  newImageAvailable = false;
  keepRunning = true;
}

void InterfazCamara::run ( )
{
  std::cout << "Esperando stream de la camara" << std::endl;
  
  // Esperar que empiece a enviar imagenes el drone
  while(!newImageAvailable)
    usleep(100000);	// 100ms
  newImageAvailable = false;
  while(!newImageAvailable)
    usleep(100000);	// 100ms
  
  // Leer la resolucion de la imagen
  frameWidth = mimFrameBW.size().x;
  frameHeight = mimFrameBW.size().y;

  mimFrameBW.resize(CVD::ImageRef(frameWidth, frameHeight));
  mimFrameBW_workingCopy.resize(CVD::ImageRef(frameWidth, frameHeight));

  snprintf(charBuf,200,"Resolucion del video: %d x %d",frameWidth,frameHeight);
  ROS_INFO(charBuf);

  // Ventana de la camara
  myGLWindow = new GLWindow2(CVD::ImageRef(frameWidth,frameHeight), "Odometria Visual", this);
  myGLWindow->set_title("Odometria Visual");

  changeSizeNextRender = true;

  if(frameWidth < 640)
    desiredWindowSize = CVD::ImageRef(frameWidth*2,frameHeight*2);
  else
    desiredWindowSize = CVD::ImageRef(frameWidth,frameHeight);

  boost::unique_lock<boost::mutex> lock(new_frame_signal_mutex);

  while(keepRunning)
  {
    if(newImageAvailable)
    {
      newImageAvailable = false;

      mimFrameBW_workingCopy.copy_from(mimFrameBW);
      mimFrameTime_workingCopy = mimFrameTime;
      mimFrameSEQ_workingCopy = mimFrameSEQ;
      mimFrameTimeRos_workingCopy = mimFrameTimeRos;

      lock.unlock();

      HandleFrame();

      if(changeSizeNextRender)
      {
	myGLWindow->set_size(desiredWindowSize);
	changeSizeNextRender = false;
      }
      
      lock.lock();
    }
    else
      new_frame_signal.wait(lock);
  }

  lock.unlock();
  delete myGLWindow;

}

void InterfazCamara::HandleFrame()
{
  msg = "";
  ros::Time startedFunc = ros::Time::now();

  myGLWindow->SetupViewport();
  myGLWindow->SetupVideoOrtho();
  myGLWindow->SetupVideoRasterPosAndZoom();


  //myGLWindow->DrawCaption("Holis");
  glClear(GL_COLOR_BUFFER_BIT);
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

  glRasterPos2i(0,0);
  glDrawPixels(mimFrameBW.size().x,mimFrameBW.size().y, GL_BGR, GL_UNSIGNED_BYTE, mimFrameBW.data());

  myGLWindow->swap_buffers();
  myGLWindow->HandlePendingEvents();


} 

void InterfazCamara::startSystem()
{
	keepRunning = true;
	changeSizeNextRender = false;
	start();
}

void InterfazCamara::stopSystem()
{
	keepRunning = false;
	new_frame_signal.notify_all();
	join();
}


void InterfazCamara::newImage(sensor_msgs::ImageConstPtr img)
{

	// Convertir msg img a cv img
	cv_bridge::CvImagePtr cv_ptr = cv_bridge::toCvCopy(img, sensor_msgs::image_encodings::MONO8);


	boost::unique_lock<boost::mutex> lock(new_frame_signal_mutex);

	if(ros::Time::now() - img->header.stamp > ros::Duration(30.0))
	  mimFrameTimeRos = (ros::Time::now()-ros::Duration(0.001));
	else
	  mimFrameTimeRos = (img->header.stamp);

	mimFrameTime = getMS(mimFrameTimeRos);

	//mimFrameTime = getMS(img->header.stamp);
	mimFrameSEQ = img->header.seq;

	// copy to mimFrame.
	// TODO: make this threadsafe (save pointer only and copy in HandleFrame)
	if(mimFrameBW.size().x != img->width || mimFrameBW.size().y != img->height)
	  mimFrameBW.resize(CVD::ImageRef(img->width, img->height));

	memcpy(mimFrameBW.data(),cv_ptr->image.data,img->width * img->height);
	newImageAvailable = true;

	lock.unlock();
	new_frame_signal.notify_all();
}


InterfazCamara::~InterfazCamara(void)
{

}
