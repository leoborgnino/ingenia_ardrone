#include "VisualOdometry.h"
#include "ros/ros.h"
#include "ros/package.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/Image.h"
#include "tf/tfMessage.h"
#include "tf/transform_listener.h"
#include "tf/transform_broadcaster.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include <ardrone_autonomy/Navdata.h>
#include "deque"
#include "std_msgs/String.h"
#include "std_msgs/Empty.h"
#include "std_srvs/Empty.h"
#include <sys/stat.h>
#include <string>

using namespace std;

VONode::VONode()
{
    //navdata_channel = nh_.resolveName("ardrone/navdata");
    //control_channel = nh_.resolveName("cmd_vel");
    video_channel = nh_.resolveName("ardrone/image_raw");
    
    //packagePath = ros::package::getPath("");

    //navdata_sub = nh_.subscribe(navdata_channel, 10, &VONode::navdataCb, this);
    //vel_sub     = nh_.subscribe(control_channel,10, &VONode::velCb, this);
    vid_sub     = nh_.subscribe(video_channel,10, &VONode::vidCb, this);

    if_camara = new InterfazCamara();

}

void VONode::Loop()
{
  ros::Rate rate(30); // FIXME
  while (nh_.ok())
  {
    ros::spinOnce();

    rate.sleep();
  }

}

void VONode::vidCb(const sensor_msgs::ImageConstPtr img)
{
  if_camara->newImage(img);
}


VONode::~VONode()
{
  delete if_camara;
}

