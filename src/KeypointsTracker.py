#! /usr/bin/env python

import cv2
import math
import rospy
import numpy as np
import os
from matplotlib import pyplot as plt

from std_msgs.msg import Float64
from std_msgs.msg import String
from std_msgs.msg import Empty

from sensor_msgs.msg import Image
from ardrone_autonomy.msg import Navdata

from cv_bridge import CvBridge

class KeypointsTracker(object):
    def __init__( self, K, undist_coeffs, detector = 'SIFT', matcher = 'FLANN'  ):

        sub_image = rospy.Subscriber("/ardrone/image_raw",Image, self.ReceiveImage)

        self.image_cnt = 0
        self.bridge = CvBridge()

        self.image_old = []
        self.started = False

        self.K = K
        self.undist_coeffs = undist_coeffs

        if ( detector == 'SIFT' ):
            self.detector = cv2.xfeatures2d.SIFT_create(700)
        elif ( detector == 'SURF' ):
            self.detector = cv2.xfeatures2d.SURF_create(700)
        elif ( detector == 'ORB' ):
            self.detector = cv2.ORB_create()

        # Flann
        if ( detector != 'ORB' ):
            FLANN_INDEX_KDTREE = 1
            index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
            search_params = dict(checks=50)
            distance = cv2.NORM_L2  # cv2.NORM_L1 cv2.NORM_L2 cv2.NORM_HAMMING(not useable) 
        else:
            
            index_params = dict(algorithm= 6,
                                table_number=6,
                                key_size=12,
                                multi_probe_level=2)
            search_params = {}
            distance = cv2.NORM_HAMMING
            
        if ( matcher == 'BF'):
            self.matcher = cv2.BFMatcher(distance, crossCheck=False)
        else:
            self.matcher = cv2.FlannBasedMatcher(index_params,search_params)

        rospy.init_node('KeypointsTracker', anonymous=True)

        rate = rospy.Rate(10) # 10 Hz
        while not rospy.is_shutdown():
            rate.sleep()


    def ReceiveImage (self, data):
        rospy.loginfo("Save %d"%(self.image_cnt))
        
        cv_image = self.bridge.imgmsg_to_cv2(data)

        if ( self.started == False ):
            self.image_old = cv_image
            self.started = True
        else:

            kp1, des1 = self.detectKPs (self.image_old)
            kp2, des2 = self.detectKPs (cv_image)
            
            matches, matchesMask = self.getMatches(des1,des2,0.9)
            
            pts1, pts2 = self.getPointsfromMatches( matches, matchesMask, kp1, kp2 )
            
            E, Emask = self.getEssentialMat(pts1, pts2)
            
            R, t = self.getRandT(E,pts1,pts2, Emask)
            
            self.drawMatches ( self.image_old, cv_image, kp1, kp2, matches, matchesMask )

            self.image_old = cv_image
            
            #plt.imshow(cv_image)
            #plt.savefig('figure%d'%(self.image_cnt))
            self.image_cnt += 1
        
    def undistort_image ( self, image ):
        return cv2.undistort(image,self.K,self.undist_coeffs)

    def detectKPs (self, image):
        kp, des = self.detector.detectAndCompute(image,None)
        return kp,des
    
    def drawKPs ( self, image, kps ):
        plt.figure()
        img=cv2.drawKeypoints(image,kps,image,flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
        plt.imshow(img)

    def getMatches ( self, descriptors1, descriptors2, threshold=0.6 ):
        matches = self.matcher.knnMatch( descriptors1, descriptors2, k = 2 )
        matchesMask = [[0,0] for i in range(len(matches))]
        
        for k,(m,n) in enumerate(matches):
            if m.distance < threshold*n.distance:
                matchesMask[k]=[1,0]

        return matches, matchesMask

    def drawMatches ( self, image1, image2, kp1, kp2, matches, mask ):
        plt.figure()
        draw_params = dict(matchColor = (0,255,0),
                           singlePointColor = (255,0,0),
                           matchesMask = mask,
                           flags = cv2.DrawMatchesFlags_DEFAULT)
        img3 = cv2.drawMatchesKnn(image1,kp1,image2,kp2,matches,None,**draw_params)
        plt.imshow(img3)
        plt.savefig('figure%d'%(self.image_cnt))

    def getPointsfromMatches ( self, matches, matchesMask, kp1, kp2 ):
        pts1 = []
        pts2 = []
        for i,(m,n) in enumerate(matches):
            if ( matchesMask[i] == [1,0] ):
                pts1.append(kp1[m.queryIdx].pt)
                pts2.append(kp2[m.trainIdx].pt)
        return pts1, pts2

    def getEssentialMat ( self, pts1, pts2 ):
        E, mask = cv2.findEssentialMat(np.array(pts1),np.array(pts2),self.K)
        return E, mask

    def getRandT ( self, E, pts1, pts2, mask ):
        retval, R, t, mask = cv2.recoverPose(E,np.array(pts1),np.array(pts2),self.K, 0.8, mask=mask)
        return R, t

if __name__ == '__main__':
    try:
        K = np.mat([[561.99,0,307.433],[0,561.78,190.144],[0,0,1]])
        dist_coeffs = np.array([-0.50758, 0.24911, 0.000579, 0.000996, 0])
        detector = 'ORB'#'SIFT'
        matcher = 'FLANN'

        kp_track = KeypointsTracker(K,dist_coeffs,detector,matcher)
    except rospy.ROSInterruptException:
        pass
